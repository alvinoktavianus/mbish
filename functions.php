<?php 

/**
 * WooCommerce Unique Code
 * --------------------------
 *
 * Tambahin kode unik pada setiap transaksi
 * Kode unik akan direset setiap 100 transaksi
 * Cukup ganti angka 100 dibawah ini untuk mengganti parameter jumlah transaksi
 *
 */
/*function woo_add_cart_fee() {
 
	global $woocommerce, $wpdb;

    $ipquery= $wpdb->get_results("SELECT COUNT(*) as code FROM wp_woocommerce_order_items GROUP BY order_id");

    $code = (int)(count($ipquery))%100;
	$woocommerce->cart->add_fee( __('Unique Code', 'woocommerce'), $code+1 );
	
}
add_action( 'woocommerce_cart_calculate_fees', 'woo_add_cart_fee' );
*/

/**
 * WooCommerce billing details
 * ----------------------------
 * Kode dibawah ini untuk customisasi billing details
 * Kode bisa dilakukan manual atau menggunakan array
 * info -> https://docs.woocommerce.com/document/tutorial-customising-checkout-fields-using-actions-and-filters/
 */
function custom_override_checkout_fields( $fields ) {
	unset($fields['billing']['billing_company']);
	unset($fields['billing']['billing_address_2']);
	unset($fields['shipping']['shipping_company']);
	$fields['billing']['billing_first_name']['label'] = 'Nama Depan';
	$fields['billing']['billing_last_name']['label'] = 'Nama Belakang';
	$fields['billing']['billing_email']['label'] = 'Alamat Email';
	$fields['billing']['billing_phone']['label'] = 'No. Handphone';
	$fields['billing']['billing_city']['label'] = 'Kota / Kabupaten';
	$fields['billing']['billing_country']['label'] = 'Negara';
	$fields['billing']['billing_state']['label'] = 'Provinsi';
	$fields['billing']['billing_postcode']['label'] = 'Kodepos';
	$fields['billing']['billing_address_1']['label'] = 'Alamat';
	$fields['billing']['billing_address_1']['placeholder'] = null;
	$fields['shipping']['shipping_first_name']['label'] = 'Nama Depan';
	$fields['shipping']['shipping_last_name']['label'] = 'Nama Belakang';
	$fields['shipping']['shipping_address_1']['label'] = 'Alamat';
	$fields['shipping']['shipping_address_2']['placeholder'] = null;
	$fields['shipping']['shipping_state']['label'] = 'Provinsi';
	$fields['shipping']['shipping_country']['label'] = 'Negara';
	$fields['shipping']['shipping_postcode']['label'] = 'Kodepos';
	$fields['order']['order_comments']['placeholder'] = 'Masukan pesan khusus (jika ada)';
	$fields['order']['order_comments']['label'] = 'Pesan Pembelanjaan';
	return $fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

?>