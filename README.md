# Mbish Child Theme

Mbish child theme adalah childing theme dari  [Media Center](https://themeforest.net/item/mediacenter-electronics-store-woocommerce-theme/9177409) untuk memenuhi kebutuhan mbish.

## Installasi

1. Download dan install Media Center.

1. Clone repo ini di folder wp-content/themes.

1. Aktifkan themes ini.

1. Pastikan plugin WooCommerce sudah terinstall pada WordPress.

1. Buatlah page dengan nama Home dan Blog. Untuk Home, pastikan template yang dipilih adalah Frontpage.

1. Klik **appearance**, **advanced options**, pastikan pada front page display dipilih **A Static Page**, lalu arahkan **Front Page** ke Home dan **Posts Page** ke Blog.

1. Lalu klik menus, pastikan struktur menu seperti ini :

    *  Home

    *  Shop

    *  Cart

    *  Checkout

    *  My Account

    *  Blog

1. Centang **Primary Menu** pada **Theme Locations**, lalu klik **Save Menu**.